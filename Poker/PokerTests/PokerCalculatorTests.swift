//
//  PokerCalculatorTests.swift
//  PokerTests
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import XCTest
@testable import Poker

class PokerCalculatorTests: XCTestCase {
    
    let calculator = PokerCalculator()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func test_sortCardsByCardValue() {

        let cards = [Card(suit: .clubs, value: .A),
                     Card(suit: .clubs, value: .two),
                     Card(suit: .clubs, value: .K),
                     Card(suit: .clubs, value: .Q),
                     Card(suit: .clubs, value: .five)]
        
        let sortedCard = calculator.sortCardsByCardValue(cards: cards)
        XCTAssertTrue(sortedCard[0] > sortedCard[1], "1st card must be higher than 2nd card")
        XCTAssertTrue(sortedCard[1] > sortedCard[2], "2st card must be higher than 3rd card")
        XCTAssertTrue(sortedCard[2] > sortedCard[3], "3rd card must be higher than 4th card")
        XCTAssertTrue(sortedCard[3] > sortedCard[4], "4th card must be higher than 5th card")
    }
}

//MARK: - PokerHandRank comparison: Same PokerHandRank
extension PokerCalculatorTests {

    func test_higherCardWithHigherCardValue_ShouldWin() {
        let higherCard1 = PokerHandRank.highCard(cards: [.A, .Q, .J, .five, .three])
        let higherCard2 = PokerHandRank.highCard(cards: [.K, .Q, .J, .five, .three])
        
        XCTAssertTrue(higherCard1 > higherCard2, "Higher Card with a higher cards value must win (1)")
        XCTAssertTrue(higherCard2 < higherCard1, "Higher Card with a higher cards value must win (2)")
        XCTAssertFalse(higherCard1 == higherCard2, "Pair with a different higher pair value must be not equal")
        
        let higherCard3 = PokerHandRank.highCard(cards: [.K, .J, .J, .five, .three])
        XCTAssertTrue(higherCard2 > higherCard3, "Pair with a higher highest cards value must win")
        
        let higherCard4 = PokerHandRank.highCard(cards: [.K, .J, .T, .five, .three])
        XCTAssertTrue(higherCard3 > higherCard4, "Pair with a higher medium cards value must win")
        
        let higherCard5 = PokerHandRank.highCard(cards: [.K, .J, .T, .four, .three])
        XCTAssertTrue(higherCard4 > higherCard5, "Pair with a higher lowest cards value must win")
        
        let higherCard6 = PokerHandRank.highCard(cards: [.K, .J, .T, .four, .two])
        XCTAssertTrue(higherCard5 > higherCard6, "Pair with a higher lowest cards value must win")
        
        let higherCard7 = PokerHandRank.highCard(cards: [.K, .J, .T, .four, .two])
        XCTAssertEqual(higherCard6, higherCard7, "Pair with same cards value must be equal")
    }
    
    func test_pairWithHigherPairCardValue_ShouldWin() {
        let pair1 = PokerHandRank.pair(pairCardValue: .three, highestCardValue: .J, mediumCardValue: .K, lowestCardValue: .A)
        let pair2 = PokerHandRank.pair(pairCardValue: .two, highestCardValue: .A, mediumCardValue: .K, lowestCardValue: .five)
        
        XCTAssertTrue(pair1 > pair2, "Pair with a higher pair cards value card must win (1)")
        XCTAssertFalse(pair1 == pair2, "Pair with a different higher pair value must be not equal")
        
        let pair3 = PokerHandRank.pair(pairCardValue: .two, highestCardValue: .K, mediumCardValue: .Q, lowestCardValue: .five)
        XCTAssertTrue(pair2 > pair3, "Pair with a higher highest cards value must win")
        
        let pair4 = PokerHandRank.pair(pairCardValue: .two, highestCardValue: .K, mediumCardValue: .J, lowestCardValue: .five)
        XCTAssertTrue(pair3 > pair4, "Pair with a higher medium cards value must win")
        
        let pair5 = PokerHandRank.pair(pairCardValue: .two, highestCardValue: .K, mediumCardValue: .J, lowestCardValue: .four)
        XCTAssertTrue(pair4 > pair5, "Pair with a higher lowest cards value must win")
        
        let pair6 = PokerHandRank.pair(pairCardValue: .two, highestCardValue: .K, mediumCardValue: .J, lowestCardValue: .four)
        XCTAssertEqual(pair5, pair6, "Pair with same pair and same card value must be equal")
    }
    
    func test_twoPairWithHigherPairCardValue_ShouldWin() {
        let twoPairs1 = PokerHandRank.twoPairs(highestPairCardValue: .three, anotherPairCardValue: .J, remainingCardValue: .five)
        let twoPairs2 = PokerHandRank.twoPairs(highestPairCardValue: .two, anotherPairCardValue: .J, remainingCardValue: .five)
        
        XCTAssertTrue(twoPairs1 > twoPairs2, "Two Pair with a higher highest pair must win (1)")
        XCTAssertFalse(twoPairs1 == twoPairs2, "Two Pair with a different higher three value must be not equal")
        
        let twoPairs3 = PokerHandRank.twoPairs(highestPairCardValue: .two, anotherPairCardValue: .T, remainingCardValue: .five)
        XCTAssertTrue(twoPairs2 > twoPairs3, "Two Pair with a higher another pair must win")
        
        let twoPairs4 = PokerHandRank.twoPairs(highestPairCardValue: .two, anotherPairCardValue: .T, remainingCardValue: .three)
        XCTAssertTrue(twoPairs3 > twoPairs4, "Two Pair with a higher remaining card value must win")
        
        let twoPairs5 = PokerHandRank.twoPairs(highestPairCardValue: .two, anotherPairCardValue: .T, remainingCardValue: .three)
        XCTAssertEqual(twoPairs4, twoPairs5, "Two Pair with same pair and same card value must be equal")
    }
    
    func test_threeOfAKindWithHigherThreeCardsValue_ShouldWin() {
        let threeOfAKind1 = PokerHandRank.threeOfAKind(threeCardsValue: .A)
        let threeOfAKind2 = PokerHandRank.threeOfAKind(threeCardsValue: .five)
        
        XCTAssertTrue(threeOfAKind1 > threeOfAKind2, "Three of a Kind with a higher three cards value must win (1)")
        XCTAssertFalse(threeOfAKind1 == threeOfAKind2, "Three of a Kind with a different higher three value must be not equal")
    }
    
    func test_straightWithHigherHighestCardValue_ShouldWin() {
        let straight1 = PokerHandRank.straight(highestCardValue: .J)
        let straight2 = PokerHandRank.straight(highestCardValue: .two)
        
        XCTAssertTrue(straight1 > straight2, "Straight with a higher highest cards value must win (1)")
        XCTAssertFalse(straight1 == straight2, "Straight with a different higher highest value must be not equal")

    }

    func test_flushWithHigherHighestCardValue_ShouldWin() {
        let flush1 = PokerHandRank.flush(cards: [.K, .Q, .T, .seven, .two])
        let flush2 = PokerHandRank.flush(cards: [.K, .Q, .nine, .seven, .two])
        
        XCTAssertTrue(flush1 > flush2, "Flush with a higher highest cards value must win (1)")
        XCTAssertFalse(flush1 == flush2, "Flush with a different higher highest value must be not equal")
        
        let flush3 = PokerHandRank.flush(cards: [.K, .Q, .nine, .seven, .two])
        XCTAssertEqual(flush2, flush3, "Flush with same cards value value must be equal")
    }
    
    func test_fullHouseWithHigherThreeCardsValue_ShouldWin() {
        let fullhouse1 = PokerHandRank.fullHouse(threeCardsValue: .three)
        let fullhouse2 = PokerHandRank.fullHouse(threeCardsValue: .two)
        
        XCTAssertTrue(fullhouse1 > fullhouse2, "Full House with a higher three cards value must win (1)")
        XCTAssertFalse(fullhouse1 == fullhouse2, "Full House with a different three cards value must be not equal")
    }
    
    func test_fourOfAKindWithHigherFourCardsValue_ShouldWin() {
        let fourOfAKind1 = PokerHandRank.fourOfAKind(fourCardsValue: .three)
        let fourOfAKind2 = PokerHandRank.fourOfAKind(fourCardsValue: .two)
        
        XCTAssertTrue(fourOfAKind1 > fourOfAKind2, "Four of a Kind with a higher four cards value must win (1)")
        XCTAssertFalse(fourOfAKind1 == fourOfAKind2, "Four of a Kind with a different four cards value must be not equal")
    }
    
    func test_straightFlushWithHigherHighestCardValue_ShouldWin() {
        let straightFlush1 = PokerHandRank.straightFlush(highestCardValue: .seven)
        let straightFlush2 = PokerHandRank.straightFlush(highestCardValue: .two)
        
        XCTAssertTrue(straightFlush1 > straightFlush2, "Straight Flush with a higher highest card must win (1)")
        XCTAssertFalse(straightFlush2 == straightFlush1, "Straight Flush with a different highest card must be not equal")
    }
}

//MARK: - PokerHandRank comparison: Different PokerHandRank
extension PokerCalculatorTests {
    
    func test_comparePairAndHigherCard_ShouldWin() {
        let pair1 = PokerHandRank.pair(pairCardValue: .two, highestCardValue: .J, mediumCardValue: .K, lowestCardValue: .A)
        let higherCard = PokerHandRank.highCard(cards: [.A, .Q, .J, .five, .two])
        XCTAssertTrue(pair1 > higherCard  , "Pairs must be higher than Higher Card")
        XCTAssertFalse(pair1 == higherCard  , "Pairs and Higher Card must be different")
    }
    
    func test_compareTwoPairsAndPair_ShouldWin() {
        let twoPairs1 = PokerHandRank.twoPairs(highestPairCardValue: .two, anotherPairCardValue: .J, remainingCardValue: .five)
        let pair1 = PokerHandRank.pair(pairCardValue: .two, highestCardValue: .J, mediumCardValue: .K, lowestCardValue: .A)
        XCTAssertTrue(twoPairs1 > pair1  , "Two Pairs must be higher than Pair")
    }
    
    func test_compareThreeOfAKindAndTwoPairs_ShouldWin() {
        let threeOfAKind1 = PokerHandRank.threeOfAKind(threeCardsValue: .Q)
        let twoPairs1 = PokerHandRank.twoPairs(highestPairCardValue: .A, anotherPairCardValue: .J, remainingCardValue: .five)
        XCTAssertTrue(threeOfAKind1 > twoPairs1  , "Three of a Kind must be higher than Two Pairs")
    }
    
    func test_compareStraightAndThreeOfAKind_ShouldWin() {
        let straight1 = PokerHandRank.straightFlush(highestCardValue: .A)
        let threeOfAKind1 = PokerHandRank.threeOfAKind(threeCardsValue: .A)
        XCTAssertTrue(straight1 > threeOfAKind1  , "Straight must be higher than Three of a Kind")
    }
    
    func test_compareFlushAndStraight_ShouldWin() {
        let flush1 = PokerHandRank.flush(cards: [.K, .Q, .T, .seven, .two])
        let straight1 = PokerHandRank.straight(highestCardValue: .K)
        XCTAssertTrue(flush1 > straight1  , "Flush must be higher than Straight")
    }
    
    func test_compareFullHouseAndFlush_ShouldWin() {
        let fullHouse1 = PokerHandRank.fullHouse(threeCardsValue: .two)
        let flush1 = PokerHandRank.flush(cards: [.K, .Q, .T, .seven, .two])
        XCTAssertTrue(fullHouse1 > flush1 , "Full House must be higher than Flush")
    }
    
    func test_compareFourOfAKindAndFullHouse_ShouldWin() {
        let fourOfAKind1 = PokerHandRank.fourOfAKind(fourCardsValue: .two)
        let fullHouse1 = PokerHandRank.fullHouse(threeCardsValue: .two)
        XCTAssertTrue(fourOfAKind1 > fullHouse1, "Four of a Kind must be higher than Full House")
        
        let fourOfAKind2 = PokerHandRank.fourOfAKind(fourCardsValue: .two)
        let fullHouse2 = PokerHandRank.fullHouse(threeCardsValue: .A)
        XCTAssertTrue(fourOfAKind2 > fullHouse2, "Four of a Kind must be higher than Full House")
    }
    
    func test_compareStraightFlushAndFourOfAKind_ShouldWin() {
        let straightFlush1 = PokerHandRank.straightFlush(highestCardValue: .two)
        let fourOfAKind1 = PokerHandRank.fourOfAKind(fourCardsValue: .two)
        
        XCTAssertTrue(straightFlush1 > fourOfAKind1, "Straight Flush must be higher than Four of a Kind (1)")
        
        /// For testing operator <
        XCTAssertTrue(fourOfAKind1 < straightFlush1, "Straight Flush must be higher than Four of a Kind (2)" )
        
        let straightFlush2 = PokerHandRank.straightFlush(highestCardValue: .two)
        let fourOfAKind2 = PokerHandRank.fourOfAKind(fourCardsValue: .A)
        XCTAssertTrue(straightFlush2 > fourOfAKind2, "Straight Flush must be higher than Four of a Kind (3)")
    }
}

//MARK: - is Same Suit
extension PokerCalculatorTests {
    
    func test_isSameSuitWithDifferentSuitCards_ShouldFail() {
        let cards = [Card(suit: .clubs, value: .A),
                     Card(suit: .diamonds, value: .K),
                     Card(suit: .clubs, value: .Q),
                     Card(suit: .spades, value: .J),
                     Card(suit: .clubs, value: .T)]
        if let isSameSuit = calculator.isSameSuit(cards: cards) {
            XCTAssertFalse(isSameSuit, "Cards contains different suits")
        } else {
            XCTFail()
        }
    }
    
    func test_isSameSuitWithSameSuitCards_ShouldSuccess() {
        let cards = [Card(suit: .clubs, value: .A),
                     Card(suit: .clubs, value: .K),
                     Card(suit: .clubs, value: .Q),
                     Card(suit: .clubs, value: .J),
                     Card(suit: .clubs, value: .T)]
        if let isSameSuit = calculator.isSameSuit(cards: cards) {
            XCTAssertTrue(isSameSuit, "Cards contains same suits")
        } else {
            XCTFail()
        }
    }
}


//MARK: - Find Poker Hand Rank
extension PokerCalculatorTests {
    
    func test_findPokerHandRankOfHighCard_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.highCardCards)
        XCTAssertEqual(rank, .highCard(cards: [.A, .Q, .J, .five, .two]),
                       "Should get High Card")
    }

    func test_findPokerHandRankOfPair_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.pairCards)
        XCTAssertEqual(rank, .pair(pairCardValue: .two, highestCardValue: .A, mediumCardValue: .Q, lowestCardValue: .J),
                       "Should get Pair")
    }

    func test_findPokerHandRankOfTwoPairs_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.twoPairCards)
        XCTAssertEqual(rank, .twoPairs(highestPairCardValue: .K, anotherPairCardValue: .two, remainingCardValue: .A),
                       "Should get Two Pair")
    }
    
    func test_findPokerHandRankOfThreeOfAKind_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.threeOfAKindCards)
        XCTAssertEqual(rank, .threeOfAKind(threeCardsValue: .two), "Should get Three Of A Kind")
    }
    
    func test_findPokerHandRankOfStraight_CorrectHighestCardValue_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.straightCards)
        XCTAssertEqual(rank, .straight(highestCardValue: .J), "Should get Straight")
    }
    
    func test_findPokerHandRankOfStraight_WrongHighestCardValue_ShouldFail() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.straightCards)
        XCTAssertNotEqual(rank, .straight(highestCardValue: .T), "It's Straight but with highest card value")
    }
    
    func test_findPokerHandRankOfFlush_CorrectCards_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.flushCards)
        XCTAssertEqual(rank, .flush(cards: [.K, .Q, .T, .seven, .two]), "Should get Flush")
    }
    
    func test_findPokerHandRankOfFlush_CorrectCardsButWithWrongOrder_ShouldSFail() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.flushCards)
        XCTAssertNotEqual(rank, .flush(cards: [.K, .Q, .T, .two, .seven]), "It's Flush but with wrong card order")
    }
    
    func test_findPokerHandRankOfFlush_WrongCards_shouldFail() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.flushCards)
        XCTAssertNotEqual(rank, .flush(cards: [.Q, .seven, .two, .K, .five]), "It's Flush but with wrong card array")
    }
    
    func test_findPokerHandRankOfFullHouse_CorrectThreeCardsValue_ShouldSucces() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.fullHoustCards)
        XCTAssertEqual(rank, .fullHouse(threeCardsValue: CardValue.Q), "Should get Full House")
    }
    
    func test_findPokerHandRankOfFullHouse_WrongThreeCardsValue_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.fullHoustCards)
        XCTAssertNotEqual(rank, .fullHouse(threeCardsValue: CardValue.A), "It's Full House but with wrong Three-Cards-Value")
    }
    
    func test_findPokerHandRankOfFourOfAKind_CorrectFourCardsValue_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.fourOfAKindCards)
        XCTAssertEqual(rank, .fourOfAKind(fourCardsValue: CardValue.two), "Should get Four of a Kind")
    }
    
    func test_findPokerHandRankOfFourOfAKind_WrongFourCardsValue_ShouldFail() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.fourOfAKindCards)
        XCTAssertNotEqual(rank, .fourOfAKind(fourCardsValue: CardValue.four), "It's Four of a Kind but with wrong Four-Cards-Value")
    }
    
    func test_findPokerHandRankOfStraightFlush_CorrectHighestCardValue_ShouldSuccess() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.straightFlushCards)
        XCTAssertEqual(rank, .straightFlush(highestCardValue: .six), "Should get Straight Flush")
    }
    
    func test_findPokerHandRankOfStraightFlush_WrongHighestCardValue_ShouldFail() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.straightFlushCards)
        XCTAssertNotEqual(rank, PokerHandRank.straightFlush(highestCardValue: .five),  "It's FStraight Flush but with wrong highest card")
    }
    
    func test_findPokerHandRank_NumberOfCardNotEqualMax_ShouldFail() {
        let rank = calculator.findPokerHandRank(cards: PokerHandFactory.invalidCards)
        XCTAssertEqual(rank, PokerHandRank.invalid, "Rank must be invalid for wrong number of cards")        
    }
}



//MARK: - is Consecutive
extension PokerCalculatorTests {
    
    func test_isConsecutiveFromHighest_ShouldSuccess() {
        let cards = [Card(suit: .clubs, value: .A),
                     Card(suit: .diamonds, value: .K),
                     Card(suit: .clubs, value: .Q),
                     Card(suit: .spades, value: .J),
                     Card(suit: .clubs, value: .T)]
        if let isConsecutive = calculator.isConsecutive(cards: cards) {
            XCTAssertTrue(isConsecutive, "Cards should be consecutive")
        } else {
            XCTFail()
        }
    }
    
    func test_isConsecutiveFromHighest_ShouldFail() {
        let cards = [Card(suit: .clubs, value: .A),
                     Card(suit: .diamonds, value: .K),
                     Card(suit: .clubs, value: .Q),
                     Card(suit: .spades, value: .J),
                     Card(suit: .clubs, value: .nine)]
        if let isConsecutive = calculator.isConsecutive(cards: cards) {
            XCTAssertFalse(isConsecutive, "Cards should be consecutive")
        } else {
            XCTFail()
        }
    }
    
    func test_isConsecutiveFromLowest_ShouldSuccess() {
        let cards = [Card(suit: .clubs, value: .two),
                     Card(suit: .diamonds, value: .three),
                     Card(suit: .clubs, value: .four),
                     Card(suit: .spades, value: .five),
                     Card(suit: .clubs, value: .six)]
        if let isConsecutive = calculator.isConsecutive(cards: cards, startFromHighest: false) {
            XCTAssertTrue(isConsecutive, "Cards should be consecutive")
        } else {
            XCTFail()
        }
    }
    
    func test_isConsecutiveFromLowest_ShouldFail() {
        let cards = [Card(suit: .diamonds, value: .three),
                     Card(suit: .clubs, value: .four),
                     Card(suit: .clubs, value: .six),
                     Card(suit: .spades, value: .seven),
                     Card(suit: .spades, value: .eight)]
        if let isConsecutive = calculator.isConsecutive(cards: cards, startFromHighest: false) {
            XCTAssertFalse(isConsecutive, "Cards should be consecutive")
        } else {
            XCTFail()
        }
    }
    
    func test_isConsecutiveWithInvalidCardCount_ShouldFail() {
        let cards = [Card]()
        if calculator.isConsecutive(cards: cards) != nil {
            XCTFail()
        }             
    }
}


//MARK: - create Group
/**
    6 possible group
 
    4, 1
    3, 2
    3, 1, 1
    2, 2, 1
    2, 1, 1, 1
    1, 1, 1, 1, 1
 */
extension PokerCalculatorTests {
    
    func test_createCardGroupsFromCardsOfHighCard() {
        
        let groups = calculator.createCardGroupsWithSortedByCountAndThenValue(cards: PokerHandFactory.highCardCards)
        
        XCTAssertEqual(groups.count, 5, "Should get 5 groups")
        
        guard groups.count == 5 else { return }
        
        XCTAssertEqual(groups[0].count, 1, "This group should have 1 count")
        XCTAssertEqual(groups[1].count, 1, "This group should have 1 count")
        XCTAssertEqual(groups[2].count, 1, "This group should have 1 count")
        XCTAssertEqual(groups[3].count, 1, "This group should have 1 count")
        XCTAssertEqual(groups[4].count, 1, "This group should have 1 count")
    }
    
    func test_createCardGroupsFromCardsOfPair() {
        let groups = calculator.createCardGroupsWithSortedByCountAndThenValue(cards: PokerHandFactory.pairCards)
        
        XCTAssertEqual(groups.count, 4, "Should get 4 groups")
        
        guard groups.count == 4 else { return }
        
        XCTAssertEqual(groups[0].count, 2, "This group should have 2 count")
        XCTAssertEqual(groups[1].count, 1, "This group should have 1 count")
        XCTAssertEqual(groups[2].count, 1, "This group should have 1 count")
        XCTAssertEqual(groups[3].count, 1, "This group should have 1 count")
    }
    
    func test_createCardGroupsFromCardsOTwoPair() {
        let groups = calculator.createCardGroupsWithSortedByCountAndThenValue(cards: PokerHandFactory.twoPairCards)
        
        XCTAssertEqual(groups.count, 3, "Should get 3 groups")
        
        guard groups.count == 3 else { return }
        
        XCTAssertEqual(groups[0].count, 2, "This group should have 2 count")
        XCTAssertEqual(groups[1].count, 2, "This group should have 2 count")
        XCTAssertEqual(groups[2].count, 1, "This group should have 1 count")
    }
    
    func test_createCardGroupsFromCardsOfThirdOfAKind() {
        let groups = calculator.createCardGroupsWithSortedByCountAndThenValue(cards: PokerHandFactory.threeOfAKindCards)
        
        XCTAssertEqual(groups.count, 3, "Should get 3 groups")
        
        guard groups.count == 3 else { return }
        
        XCTAssertEqual(groups[0].count, 3, "This group should have 3 count")
        XCTAssertEqual(groups[1].count, 1, "This group should have 1 count")
        XCTAssertEqual(groups[2].count, 1, "This group should have 1 count")
    }
    
    func test_createCardGroupsFromCardsOfFullHouse() {
        let groups = calculator.createCardGroupsWithSortedByCountAndThenValue(cards: PokerHandFactory.fullHoustCards)
        
        XCTAssertEqual(groups.count, 2, "Should get 2 groups")
        
        guard groups.count == 2 else { return }
        
        XCTAssertEqual(groups[0].count, 3, "This group should have 3 count")
        XCTAssertEqual(groups[1].count, 2, "This group should have 2 count")
    }
    
    func test_createCardGroupsFromCardsOfForFourOfAKind() {
        let groups = calculator.createCardGroupsWithSortedByCountAndThenValue(cards: PokerHandFactory.fourOfAKindCards)
        
        XCTAssertEqual(groups.count, 2, "Should get 2 groups")
        
        guard groups.count == 2 else { return }
        
        XCTAssertEqual(groups[0].count, 4, "This group should have 4 count")
        XCTAssertEqual(groups[1].count, 1, "This group should have 1 count")
    }
}


