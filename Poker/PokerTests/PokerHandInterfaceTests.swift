//
//  PokerHandInterfaceTests.swift
//  PokerTests
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import XCTest
@testable import Poker


class PokerHandInterfaceTests: XCTestCase {
    
    let player = PokerPlayer(id: "00001")
    let pokerHand = PokerHand()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_addCardToPokerHandSuccess() {
        let addResult = pokerHand.addCard(card: Card(suit: .diamonds, value: .two))
        XCTAssertTrue(addResult, "Fail to add card to Poker Hand")
    }
    
    func test_addCardToPokerHandWithDuplicatedCard_ShouldFail() {
        _ = pokerHand.addCard(card: Card(suit: .diamonds, value: .two))
        let addResult = pokerHand.addCard(card: Card(suit: .diamonds, value: .two))
        XCTAssertFalse(addResult, "Fail to detect a duplicated card added to Poker Hand")
    }
    
    func test_addCardToPokerHandExceed5Cards_ShouldFail() {
        _ = pokerHand.addCard(card: Card(suit: .diamonds, value: .two))
        _ = pokerHand.addCard(card: Card(suit: .diamonds, value: .three))
        _ = pokerHand.addCard(card: Card(suit: .diamonds, value: .four))
        _ = pokerHand.addCard(card: Card(suit: .diamonds, value: .five))
        _ = pokerHand.addCard(card: Card(suit: .diamonds, value: .six))
        
        let addResult = pokerHand.addCard(card: Card(suit: .diamonds, value: .two))
        XCTAssertFalse(addResult, "Fail to detect the exceed number of cards added to Poker Hand")
    }
    
    func test_addCardsArray_ShouldSuccess() {
        
        let addResult1 = pokerHand.addCards(cards: [Card(suit: .diamonds, value: .two),
                                                    Card(suit: .diamonds, value: .three),
                                                    Card(suit: .diamonds, value: .four)] )
        XCTAssertTrue(addResult1, "")
        
        let addResult2 = pokerHand.addCards(cards: [Card(suit: .diamonds, value: .six),
                                                    Card(suit: .diamonds, value: .five)] )
        XCTAssertTrue(addResult2, "")
        
        let addResult3 = pokerHand.addCards(cards: [Card(suit: .diamonds, value: .A)] )
        XCTAssertFalse(addResult3, "Fail to detect the exceed number of cards added to Poker Hand")
    }
    
    func test_removeCardFromPokerHandSuccess() {
        let card = Card(suit: .diamonds, value: .two)
        _ = pokerHand.addCard(card: card)

        XCTAssertNotNil(pokerHand.cards.first, "Poker Hand must contain one card")
        
        pokerHand.removeCard(card: card)
        
        XCTAssertNil(pokerHand.cards.first, "Poker Hand must not contain any card")
    }
    
    func test_removeAllCardFromPokerHandSuccess() {
        let card = Card(suit: .diamonds, value: .two)
        _ = pokerHand.addCard(card: card)
        
        XCTAssertNotNil(pokerHand.cards.first, "Poker Hand must contain one card")
        
        pokerHand.removeAllCards()
        
        XCTAssertNil(pokerHand.cards.first, "Poker Hand must not contain any card")
    }
}
