//
//  SuitTests.swift
//  PokerTests
//
//  Created by ben on 18/3/2561 BE.
//  Copyright © 2561 ben. All rights reserved.
//

import XCTest

@testable import Poker

class SuitTests: XCTestCase {
    
    func test_SuitDescriptionText() {
        XCTAssertEqual("\(Suit.clubs)", "Clubs", "Suit description text is wrong")
        XCTAssertEqual("\(Suit.diamonds)", "Diamonds", "Suit description text is wrong")
        XCTAssertEqual("\(Suit.hearts)", "Hearts", "Suit description text is wrong")
        XCTAssertEqual("\(Suit.spades)", "Spades", "Suit description text is wrong")
    }
}
