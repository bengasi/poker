//
//  DeckInterfaceTests.swift
//  PokerTests
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import XCTest
@testable import Poker

class DeckInterfaceTests: XCTestCase {

    let deck = DeckFactory.getDeck(type: DeckType.deckWith52Cards)
    var expectedNumberOfCardsInDeck: Int {
        return deck.expectedCardsCount
    }
    
    func test_getDeckWith52cards() {
        let cardsCount = deck.cards.count
        XCTAssertEqual(cardsCount, expectedNumberOfCardsInDeck, "Wrong number of cards in deck")
    }
    
    func test_dealCardFromDeck() {
        let expectedCard = Card(suit: .clubs, value: .J)
        
        if let card = deck.dealCard(suit: .clubs, value: .J) {
            XCTAssertEqual(card, expectedCard, "Deck dealt the wrong card \(card)")
        } else {
            XCTFail("Deck cannot deal card")
        }
    }
    
    func test_dealCardFromDeckTwice_ShouldFail() {
        if let _ = deck.dealCard(suit: .clubs, value: .J) {
            let nilCard = deck.dealCard(suit: .clubs, value: .J)
            XCTAssertNil(nilCard, "Card dealt twice is wrong")
        } else {
            XCTFail("Deck cannot deal card")
        }
    }
    
    func test_isCardDealtForDealtCard_ShouldTrue() {
        if let card = deck.dealCard(suit: .clubs, value: .J) {
            XCTAssertTrue(deck.isCardDealt(card: card), "The card was already dealt")
        } else {
            XCTFail("Deck cannot deal card")
        }
    }
    
    func test_isCardDealtForUndealtCard_ShouldFalse() {
        XCTAssertFalse(deck.isCardDealt(card: Card(suit: .clubs, value: .K)), "The card was not been dealt")
    }
    
    func test_getUndealtCards() {

        // Ensure that before dealing the card, the number of cards is correct
        XCTAssertEqual(deck.getUndealtCards().count, expectedNumberOfCardsInDeck, "No cards is dealt")
        
        // Ensure that after dealing the card, the number of cards is still correct
        if let _ = deck.dealCard(suit: .clubs, value: .J) {
            XCTAssertEqual(deck.getUndealtCards().count, expectedNumberOfCardsInDeck - 1, "No cards is dealt")
        } else {
            XCTFail("Deck cannot deal card")
        }
    }
}
