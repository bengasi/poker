//
//  PokerDeckViewModelTest.swift
//  PokerTests
//
//  Created by ben on 18/3/2561 BE.
//  Copyright © 2561 ben. All rights reserved.
//

import XCTest
@testable import Poker

class PokerDeckViewModelTest: XCTestCase {
    
    let viewModel: PokerDeckInteractorInterface = PokerDeckViewModel(playerIndex: 0)
    
   
    func test_updateCardIndex() {
        viewModel.input.updateCardIndex(2)
        XCTAssertEqual(viewModel.output.cardIndex, 2, "")
        
        viewModel.input.updateCardIndex(4)
        XCTAssertEqual(viewModel.output.cardIndex, 4, "")
        
        //viewModel.input.updateCard(card: Card(suit: Suit.clubs, value: CardValue.A))
    }
    
    func test_updateCard() {
        let card = Card(suit: Suit.clubs, value: CardValue.A)
        viewModel.input.updateCardIndex(2)
        viewModel.input.updateCard(card: card)
        
        let cards =  viewModel.output.cards
        XCTAssertEqual(cards[0], nil, "")
        XCTAssertEqual(cards[1], nil, "")
        XCTAssertEqual(cards[2], card, "")
    
    }
    
    func test_updateCardWithInvalidIndex_ShouldFail() {
        let card = Card(suit: Suit.clubs, value: CardValue.A)
        viewModel.input.updateCardIndex(55)
        viewModel.input.updateCard(card: card)
        
        let cards =  viewModel.output.cards
        cards.forEach { (card) in
            XCTAssertNil(card, "All card must be nil")
        }
    }
}
