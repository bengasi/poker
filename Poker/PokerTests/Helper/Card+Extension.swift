//
//  Card+Extension.swift
//  PokerTests
//
//  Created by ben on 1/24/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation
@testable import Poker

extension Card {
    
    init(valueStr: String, suitStr: String) {
        if valueStr == "2" { value = .two }
        else if valueStr == "3" { value = .three }
        else if valueStr == "4" { value = .four }
        else if valueStr == "5" { value = .five }
        else if valueStr == "6" { value = .six }
        else if valueStr == "7" { value = .seven }
        else if valueStr == "8" { value = .eight }
        else if valueStr == "9" { value = .nine }
        else if valueStr == "T" { value = .T }
        else if valueStr == "J" { value = .J }
        else if valueStr == "Q" { value = .Q }
        else if valueStr == "K" { value = .K }
        else { value = .A }
        
        if suitStr == "C" { suit = .clubs }
        else if suitStr == "D" { suit = .diamonds }
        else if suitStr == "H" { suit = .spades }
        else { suit = .hearts }
    }
}
