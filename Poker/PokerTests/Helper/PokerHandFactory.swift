//
//  PokerHandFactory.swift
//  PokerTests
//
//  Created by ben on 1/24/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation
@testable import Poker

class PokerHandFactory {
    static let highCardCards = [Card(suit: .clubs, value: .two),
                                Card(suit: .diamonds, value: .five),
                                Card(suit: .clubs, value: .J),
                                Card(suit: .spades, value: .Q),
                                Card(suit: .clubs, value: .A)]
    
    static let pairCards = [Card(suit: .clubs, value: .two),
                            Card(suit: .diamonds, value: .two),
                            Card(suit: .clubs, value: .J),
                            Card(suit: .spades, value: .Q),
                            Card(suit: .clubs, value: .A)]
    
    static let threeOfAKindCards = [Card(suit: .clubs, value: .two),
                                    Card(suit: .spades, value: .two),
                                    Card(suit: .diamonds, value: .two),
                                    Card(suit: .spades, value: .five),
                                    Card(suit: .clubs, value: .K)]
    
    static let twoPairCards = [Card(suit: .clubs, value: .two),
                               Card(suit: .clubs, value: .K),
                               Card(suit: .diamonds, value: .two),
                               Card(suit: .spades, value: .K),
                               Card(suit: .clubs, value: .A)]
    
    static let straightCards = [Card(suit: .clubs, value: .seven),
                                Card(suit: .spades, value: .T),
                                Card(suit: .diamonds, value: .nine),
                                Card(suit: .spades, value: .eight),
                                Card(suit: .clubs, value: .J)]
    
    static let flushCards = [Card(suit: .spades, value: .Q),
                             Card(suit: .spades, value: .seven),
                             Card(suit: .spades, value: .two),
                             Card(suit: .spades, value: .K),
                             Card(suit: .spades, value: .T)]
    
    static let fullHoustCards = [Card(suit: .clubs, value: .Q),
                                 Card(suit: .spades, value: .Q),
                                 Card(suit: .diamonds, value: .Q),
                                 Card(suit: .spades, value: .A),
                                 Card(suit: .clubs, value: .A)]
    
    static let fourOfAKindCards = [Card(suit: .clubs, value: .two),
                                   Card(suit: .spades, value: .two),
                                   Card(suit: .diamonds, value: .two),
                                   Card(suit: .hearts, value: .two),
                                   Card(suit: .clubs, value: .K)]
    
    static let straightFlushCards = [Card(suit: .clubs, value: .two),
                                     Card(suit: .clubs, value: .four),
                                     Card(suit: .clubs, value: .three),
                                     Card(suit: .clubs, value: .five),
                                     Card(suit: .clubs, value: .six)]
    
    static let invalidCards = [Card(suit: .clubs, value: .two),
                                     Card(suit: .clubs, value: .four),
                                     Card(suit: .clubs, value: .three),
                                     Card(suit: .clubs, value: .five)]
}

