//
//  CardTests.swift
//  PokerTests
//
//  Created by ben on 18/3/2561 BE.
//  Copyright © 2561 ben. All rights reserved.
//

import XCTest

@testable import Poker

class CardTests: XCTestCase {
    let cardWithTwoValue = Card(suit: Suit.hearts, value: CardValue.two)
    let cardWithThreeValue = Card(suit: Suit.hearts, value: CardValue.three)
    
    func test_cardShownText() {
        XCTAssertEqual(CardValue.two.shownText, "2", "")
        XCTAssertEqual(CardValue.three.shownText, "3", "")
        XCTAssertEqual(CardValue.four.shownText, "4", "")
        XCTAssertEqual(CardValue.five.shownText, "5", "")
        XCTAssertEqual(CardValue.six.shownText, "6", "")
        XCTAssertEqual(CardValue.seven.shownText, "7", "")
        XCTAssertEqual(CardValue.eight.shownText, "8", "")
        XCTAssertEqual(CardValue.nine.shownText, "9", "")
        XCTAssertEqual(CardValue.T.shownText, "10", "")
        XCTAssertEqual(CardValue.J.shownText, "J", "")
        XCTAssertEqual(CardValue.Q.shownText, "Q", "")
        XCTAssertEqual(CardValue.K.shownText, "K", "")
        XCTAssertEqual(CardValue.A.shownText, "A", "")
    }
  
    func test_cardValueText() {
        let card2 = Card.init(suit: Suit.hearts, value: CardValue.two)
        XCTAssertEqual(card2.valueText, "2", "")
    }
    
    func test_lessThanOperator() {
        XCTAssertTrue(cardWithTwoValue < cardWithThreeValue, "")
    }
    
    func test_greaterThanOperator() {
         XCTAssertTrue(cardWithThreeValue > cardWithTwoValue, "")
    }
    
    func test_cardImageName() {
        let cardC = Card.init(suit: Suit.clubs, value: CardValue.two)
        let cardD = Card.init(suit: Suit.diamonds, value: CardValue.two)
        let cardH = Card.init(suit: Suit.hearts, value: CardValue.two)
        let cardS = Card.init(suit: Suit.spades, value: CardValue.two)
        
        XCTAssertEqual(cardC.imageName, "clubs_filled", "")
        XCTAssertEqual(cardD.imageName, "diamonds_filled", "")
        XCTAssertEqual(cardH.imageName, "hearts_filled", "")
        XCTAssertEqual(cardS.imageName, "spades_filled", "")
    }
}
