//
//  PokerBoardViewModelTests.swift
//  PokerTests
//
//  Created by ben on 18/3/2561 BE.
//  Copyright © 2561 ben. All rights reserved.
//

import XCTest

@testable import Poker

class PokerBoardViewModelTests: XCTestCase {
    
    var viewModel: PokerBoardViewModel!
    
    override func setUp() {
        super.setUp()
        
        let p1 = PokerPlayer(id: "001")
        p1.name = "p1"
        
        let p2 = PokerPlayer(id: "002")
        p2.name = "p2"
        
        viewModel = PokerBoardViewModel(service: PokerWithTwoHandsCalculatorService.init(calculator: PokerCalculator()), player1: p1, player2: p2)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_updateCard() {
        let cards = PokerHandFactory.highCardCards
        viewModel.input.updateCardsForPlayer(index: 0, cards: cards)
        
        let outputCards = viewModel.output.getCardInfo(playerIndex: 0)
        XCTAssertEqual(outputCards, cards, "")
    }
    
    func test_getCardInfoOfWrongPlayerIndex_ShouldFail() {
        let cards = PokerHandFactory.highCardCards
        viewModel.input.updateCardsForPlayer(index: 0, cards: cards)
        
        let outputCards = viewModel.output.getCardInfo(playerIndex: 5)
        XCTAssertEqual(outputCards, [], "")
    }
    
    func test_updateCardsForWrongPlayerIndex_ShouldFail() {
        
        let cards = PokerHandFactory.highCardCards
        viewModel.input.updateCardsForPlayer(index: 3, cards: cards)
        
        let outputCards = viewModel.output.getCardInfo(playerIndex: 3)
        XCTAssertEqual(outputCards, [], "")
    }
    
    func test_calculateGetWin() {
        let cards1 = PokerHandFactory.highCardCards
        let cards2 = PokerHandFactory.flushCards
        
        viewModel.input.updateCardsForPlayer(index: 0, cards: cards1)
        viewModel.input.updateCardsForPlayer(index: 1, cards: cards2)
        viewModel.input.calculatePokerHand()
        
        let outputString = viewModel.output.getGameResult()
        
        XCTAssertEqual(outputString, "p2 win with Flush", "")
    }
    
    func test_calculateGetTie() {
        viewModel.input.updateCardsForPlayer(index: 0, cards: PokerHandFactory.flushCards)
        viewModel.input.updateCardsForPlayer(index: 1, cards: PokerHandFactory.flushCards)
        viewModel.input.calculatePokerHand()
        
        let outputString = viewModel.output.getGameResult()
        
        XCTAssertEqual(outputString, "Tie with Flush", "")
        
        let highCards = PokerHandFactory.highCardCards
        
        viewModel.input.updateCardsForPlayer(index: 0, cards: highCards)
        viewModel.input.updateCardsForPlayer(index: 1, cards: highCards)
        viewModel.input.calculatePokerHand()
        
        let outputString2 = viewModel.output.getGameResult()
        
        XCTAssertEqual(outputString2, "Tie with High Card", "")
    }
    
    func test_calculateGetWinWithAttribute() {
        let flushCards1 = [Card(suit: .spades, value: .Q),
                           Card(suit: .spades, value: .seven),
                           Card(suit: .spades, value: .three), // Three
                           Card(suit: .spades, value: .K),
                           Card(suit: .spades, value: .T)]
        let flushCards2 = [Card(suit: .spades, value: .Q),
                           Card(suit: .spades, value: .seven),
                           Card(suit: .spades, value: .two),   // Two
                           Card(suit: .spades, value: .K),
                           Card(suit: .spades, value: .T)]
        
        viewModel.input.updateCardsForPlayer(index: 0, cards: flushCards1)
        viewModel.input.updateCardsForPlayer(index: 1, cards: flushCards2)
        viewModel.input.calculatePokerHand()
        
        let outputString = viewModel.output.getGameResult()
        
        XCTAssertEqual(outputString, "p1 win with Flush: 3", "")
    }
    
    func test_getGameResultWithoutCalculate_ShouldFail() {
        viewModel.input.updateCardsForPlayer(index: 0, cards: PokerHandFactory.flushCards)
        viewModel.input.updateCardsForPlayer(index: 1, cards: PokerHandFactory.flushCards)
        
        let outputString = viewModel.output.getGameResult()
        
        XCTAssertEqual(outputString, "", "")
    }
    
    func test_calculateForInvalidNumberOfCard_ShouldFail() {
        let cards = [Card(suit: .spades, value: .Q),
                     Card(suit: .spades, value: .seven),
                     Card(suit: .spades, value: .two),
                     Card(suit: .spades, value: .K)]
        
        viewModel.input.updateCardsForPlayer(index: 0, cards: cards)
        viewModel.input.updateCardsForPlayer(index: 1, cards: PokerHandFactory.flushCards)
        viewModel.input.calculatePokerHand()
        let outputString = viewModel.output.getGameResult()
        
        XCTAssertEqual(outputString, "", "")
    }
}
