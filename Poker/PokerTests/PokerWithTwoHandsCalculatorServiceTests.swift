//
//  PokerWithTwoHandsCalculatorServiceTests.swift
//  PokerTests
//
//  Created by ben on 18/3/2561 BE.
//  Copyright © 2561 ben. All rights reserved.
//

import XCTest

@testable import Poker

class PokerWithTwoHandsCalculatorServiceTests: XCTestCase {
    
    var service: PokerCalculatorServiceInterface!
    
    override func setUp() {
        service = PokerWithTwoHandsCalculatorService.init(calculator: PokerCalculator())
    }
    
    func test_wrongNumberOfCardArray_shouldFail() {
        let result1 = service.calculate(pokerHands: [])
        XCTAssertNil(result1.comparisionResult, "Comparision result should be nil ")
        XCTAssertNil(result1.winPokerHandRank, "Win should be nil ")
        XCTAssertNil(result1.winAttributes, "Win attributes should be nil ")
        XCTAssertNil(result1.losePokerHandRank, "Lose should be nil ")
        XCTAssertEqual(result1.winIndex, -1, "Win index should be -1")
        XCTAssertFalse(result1.isSameRankType, "Should Get false for same rank type")
    }
    
    func test_comparePairWinHighCard() {
        
        let pair1 = [Card(suit: .clubs, value: .two),
                     Card(suit: .diamonds, value: .two),
                     Card(suit: .clubs, value: .J),
                     Card(suit: .spades, value: .Q),
                     Card(suit: .clubs, value: .A)]
        let higherCard = [Card(suit: .clubs, value: .two),
                             Card(suit: .diamonds, value: .five),
                             Card(suit: .clubs, value: .J),
                             Card(suit: .spades, value: .Q),
                             Card(suit: .clubs, value: .A)]
        
        let result1 = service.calculate(pokerHands: [pair1, higherCard])
        
        XCTAssertEqual(result1.comparisionResult, .win, "")
        XCTAssertEqual(result1.winIndex, 0, "Win index should be 0")
        XCTAssertFalse(result1.isSameRankType, "Should Get false for same rank type")
        
        if case PokerHandRank.pair(let pairCardValue, let highestCardValue, let mediumCardValue, let lowestCardValue)? = result1.winPokerHandRank {
            XCTAssertEqual(pairCardValue, CardValue.two, "")
            XCTAssertEqual(highestCardValue, CardValue.A, "")
            XCTAssertEqual(mediumCardValue, CardValue.Q, "")
            XCTAssertEqual(lowestCardValue, CardValue.J, "")
        } else {
            XCTFail()
        }
        
        if case PokerHandRank.highCard(let cards)? = result1.losePokerHandRank {
            XCTAssertEqual(cards[0], CardValue.A, "")
            XCTAssertEqual(cards[1], CardValue.Q, "")
            XCTAssertEqual(cards[2], CardValue.J, "")
            XCTAssertEqual(cards[3], CardValue.five, "")
            XCTAssertEqual(cards[4], CardValue.two, "")
        } else {
            XCTFail()
        }
        
        XCTAssertEqual(result1.winAttributes, nil, "")
    }
}
