//
//  PokerCalculatorTest+InputOutputFile.swift
//  PokerTests
//
//  Created by ben on 1/24/18.
//  Copyright © 2018 ben. All rights reserved.
//

import XCTest
@testable import Poker

class PokerCalculatorTest_InputOutputFile: XCTestCase {
    
    let calculator = PokerCalculator()
    let inputReader = InputOutputReader()
    
    func test_calculateGameFromInputOutputFile() {
        for (index, game) in inputReader.inputs.enumerated() {
            let blackRank = calculator.findPokerHandRank(cards: game[0])
            let whiteRank = calculator.findPokerHandRank(cards: game[1])
            let result = calculator.compare(pokerHandRank1: blackRank, pokerHandRank2: whiteRank)
            //print("result: \(index) \(result)")
            XCTAssertEqual(result, inputReader.outputs[index], "Game result is wrong at \(index) \n\(blackRank) \n\(whiteRank)")
        }
    }
    
    func test_calculateAndGetDetailFromGameFromInputOutputFile() {
        
        for (index, game) in inputReader.inputs.enumerated() {
            let blackRank = calculator.findPokerHandRank(cards: game[0])
            let whiteRank = calculator.findPokerHandRank(cards: game[1])
            let result = calculator.detailCompare(pokerHandRank1: blackRank, pokerHandRank2: whiteRank)
            print("result2: \(index) \(result)\n")
            
            // Test property comparision Result
            XCTAssertEqual(result.comparisionResult, inputReader.outputs[index], "Game result is wrong at \(index) \n\(blackRank) \n\(whiteRank)")
            
            // Test property winIndex, winPokerHandRank, losePokerHandRank, isSameRankType
            
            if index == 4 {
                XCTAssertEqual(result.winIndex, 1, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, .six, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 9 {
                XCTAssertEqual(result.winIndex, 0, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, nil, "Wrong winAttributes")
                XCTAssertFalse(result.isSameRankType, "Must be same rank type")
            }else if index == 14 {
                XCTAssertEqual(result.winIndex, 1, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, .A, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 19 {
                XCTAssertEqual(result.winIndex, 0, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, .T, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 24 {
                XCTAssertEqual(result.winIndex, -1, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winPokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong losePokerHandRank")
                XCTAssertEqual(result.winAttributes, nil, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 29 {
                XCTAssertEqual(result.winIndex, 0, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, .five, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 34 {
                XCTAssertEqual(result.winIndex, 0, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, nil, "Wrong winAttributes")
                XCTAssertFalse(result.isSameRankType, "Must be same rank type")
            }else if index == 39 {
                XCTAssertEqual(result.winIndex, -1, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winPokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong losePokerHandRank")
                XCTAssertEqual(result.winAttributes, nil, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 44 {
                XCTAssertEqual(result.winIndex, -1, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winPokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong losePokerHandRank")
                XCTAssertEqual(result.winAttributes, nil, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 49 {
                XCTAssertEqual(result.winIndex, 0, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, .seven, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }else if index == 54 {
                XCTAssertEqual(result.winIndex, 0, "Wrong winIndex")
                XCTAssertEqual(result.winPokerHandRank, blackRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.losePokerHandRank, whiteRank, "Wrong winPokerHandRank")
                XCTAssertEqual(result.winAttributes, .five, "Wrong winAttributes")
                XCTAssertTrue(result.isSameRankType, "Must be same rank type")
            }
        }
    }
}
