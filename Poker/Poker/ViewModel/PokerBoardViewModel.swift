//
//  PokerBoardViewModel.swift
//  Poker
//
//  Created by ben on 1/25/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


// MARK: - Input
protocol PokerBoardInteractorInput {
    func calculatePokerHand()
    func updateCardsForPlayer(index: Int, cards: [Card])
}


// MARK: - Output
protocol PokerBoardInteractorOutput : class {
    var didUpdateCalculation: (() -> Void)? { get set }
    var didUpdatePlayerPokerHand: ((_ index: Int) -> Void)? { get set }
    
    func getPlayer(index: Int) -> PokerPlayer
    func getCardInfo(playerIndex: Int) -> [Card]
    func getGameResult() -> String
}


// MARK: - Interface
protocol PokerBoardInteractorInterface {
    var input: PokerBoardInteractorInput { get }
    var output: PokerBoardInteractorOutput { get }
}


class PokerBoardViewModel: PokerBoardInteractorInterface, PokerBoardInteractorOutput {

    var input: PokerBoardInteractorInput { return self }
    var output: PokerBoardInteractorOutput { return self }

    var didUpdateCalculation: (() -> Void)?
     var didUpdatePlayerPokerHand: ((Int) -> Void)?
    
    private var players = [PokerPlayer]()
    private var service: PokerCalculatorServiceInterface
    private var latestCalculationResult: PokerCalculationResult?
    
    init(service: PokerCalculatorServiceInterface, player1: PokerPlayer, player2: PokerPlayer) {
        self.service = service
        initializeGame(player1: player1, player2: player2)
    }
    
    private func initializeGame(player1: PokerPlayer, player2: PokerPlayer) {
        players.append(player1)
        players.append(player2)
    }
    
    func getPlayer(index: Int) -> PokerPlayer {
        return players[index]
    }
    
    func getCardInfo(playerIndex index: Int) -> [Card] {
        guard index < players.count else { return [] }
        let player = players[index]
        
        return player.pokerHand.cards
    }
    
    /**
        Get text result in format
        - Player 1 win with Four of a Kind: A
        - Player 2 win with Straigt
        - Tie with Pair
     */
    func getGameResult() -> String {
        var formatText = ""
        guard let calculationResult = latestCalculationResult else { return formatText }
        guard let pokerComparison = calculationResult.comparisionResult else { return formatText }
        print("calculationResult \(calculationResult)")
        let winIndex = calculationResult.winIndex
        
        if (pokerComparison == .win || pokerComparison == .lose),
            let type = calculationResult.winPokerHandRank?.description {
            if let attribute = calculationResult.winAttributes {
                formatText = "\(players[winIndex].name) win with \(type): \(attribute.shownText)"
            } else {
                formatText = "\(players[winIndex].name) win with \(type)"
            }
            
        } else if pokerComparison == .tie, let rank = calculationResult.winPokerHandRank {
            formatText = "Tie with \(rank.description)"
        }
        return formatText
    }
}

extension PokerBoardViewModel: PokerBoardInteractorInput {
    
    func updateCardsForPlayer(index: Int, cards: [Card]) {
        
        guard index < players.count else { return }
        
        let player = players[index]
        _ = player.pokerHand.removeAllCards()
        _ = player.pokerHand.addCards(cards: cards)
        
        didUpdatePlayerPokerHand?(index)
    }
    
    func calculatePokerHand() {
        let pokerHand1 = players[0].pokerHand
        let pokerHand2 = players[1].pokerHand
        
        guard pokerHand1.cards.count == 5, pokerHand2.cards.count == 5 else { return }
        
        let result: PokerCalculationResult = service.calculate(pokerHands: [pokerHand1.cards, pokerHand2.cards])
        
        latestCalculationResult = result
        
        print("Latest = \(String(describing: latestCalculationResult))")
        didUpdateCalculation?()
    }
}


//MARK
extension Card {
    
    var valueText: String {
        return value.shownText
    }
    
    var imageName: String {
        switch suit {
        case .clubs:
            return "clubs_filled"
        case .diamonds:
            return "diamonds_filled"
        case .hearts:
            return "hearts_filled"
        case .spades:
            return "spades_filled"
        }
    }
}
