//
//  PokerDeckViewModel.swift
//  Poker
//
//  Created by Atikom Tancharoen on 26/1/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


// MARK: - Input
protocol PokerDeckInteractorInput {
    func updateCardIndex(_ index: Int)
    func updateCard(card: Card)
}


// MARK: - Output
protocol PokerDeckInteractorOutput : class {
    var playerIndex: Int { get }
    var cards: [Card?] { get }
    var cardIndex: Int { get }
    
    var didUpdateSelectedCard: ((_ index: Int) -> Void)? { get set }
}


// MARK: - Interface
protocol PokerDeckInteractorInterface {
    var input: PokerDeckInteractorInput { get }
    var output: PokerDeckInteractorOutput { get }
}


class PokerDeckViewModel: PokerDeckInteractorInterface, PokerDeckInteractorOutput {
    
    var input: PokerDeckInteractorInput { return self }
    var output: PokerDeckInteractorOutput { return self }
    
    var cards: [Card?] = [nil, nil, nil, nil, nil]
    var playerIndex: Int = 0
    var cardIndex: Int = 0
    
    var didUpdateSelectedCard: ((Int) -> Void)?
    
    init(playerIndex: Int) {
        self.playerIndex = playerIndex
    }
}


extension PokerDeckViewModel: PokerDeckInteractorInput {
    
    func updateCardIndex(_ index: Int) {
        cardIndex = index
    }

    func updateCard(card: Card) {
        guard cardIndex < cards.count else { return }
        
        cards[cardIndex] = card
        didUpdateSelectedCard?(cardIndex)
    }    
}
