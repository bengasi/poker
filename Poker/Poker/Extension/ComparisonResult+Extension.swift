//
//  ComparisonResult+Extension.swift
//  Poker
//
//  Created by ben on 1/24/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


extension ComparisonResult {
    /**
        Convert from ComparisonResult to PokerComparisonResult
     */
    func toPokerComparasionResult() -> PokerComparisonResult {
        switch self {
        case .orderedSame:  return .tie
        case .orderedDescending: return .win
        case .orderedAscending: return .lose
        }
    }
}
