//
//  CardView.swift
//  Poker
//
//  Created by ben on 1/25/18.
//  Copyright © 2018 ben. All rights reserved.
//

import UIKit

class CardView: UIView {

    @IBOutlet private weak var value: UILabel!
    @IBOutlet private weak var suitImageView: UIImageView!

    func configure(valueText: String, imageName: String) {
        value.text = valueText
        suitImageView.image = UIImage(named: imageName)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
}
