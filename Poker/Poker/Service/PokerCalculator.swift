//
//  PokerCalculator.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


extension Sequence where Iterator.Element == Card {
    
    func sort() -> [Card] {
        return sorted {$0 > $1}
    }
}


protocol PokerCalculatorInterface {
    var maxNumberOfCards: Int { get }
    func compare(pokerHandRank1: PokerHandRank, pokerHandRank2: PokerHandRank) -> PokerComparisonResult
    func detailCompare(pokerHandRank1: PokerHandRank, pokerHandRank2: PokerHandRank) -> PokerCalculationResult
    func findPokerHandRank(cards: [Card]) -> PokerHandRank
}

class PokerCalculator: PokerCalculatorInterface {
    
    let maxNumberOfCards: Int = 5
    
    /// Find out which if the first PokerHandRank is win, lose, or tie against the second PokerHandRank
    func compare(pokerHandRank1: PokerHandRank, pokerHandRank2: PokerHandRank) -> PokerComparisonResult {
        return pokerHandRank1.compareToAnother(another: pokerHandRank2)
    }
    
    /**
        Find out if the first PokerHandRank is win, lose, or tie against the second PokerHandRank.
        Also, see an attribute of a win PokerHandRank
     */
    func detailCompare(pokerHandRank1: PokerHandRank, pokerHandRank2: PokerHandRank) -> PokerCalculationResult {
        return pokerHandRank1.compareToAnotherAndGetDetail(another: pokerHandRank2)
    }

    /**
        Find the type of Poker Hand. The calculation consists of the following validation
        - a number of card in the poker hand must be less than or equal 5
        - invalid when checking if they are in the same suit
        - invalid when checking if they are is in consecutive order
     
     */
    func findPokerHandRank(cards: [Card]) -> PokerHandRank {
        
        guard cards.count == maxNumberOfCards else { return .invalid}
        
        let sortedCard = sortCardsByCardValue(cards: cards)
        
        guard let isSameSuit = isSameSuit(cards: sortedCard) else { return .invalid }
        guard let isConsecutive = isConsecutive(cards: sortedCard) else { return .invalid }
        guard let firstCard = sortedCard.first else { return .invalid}                    
        
        if isSameSuit {
            if isConsecutive {
                return .straightFlush(highestCardValue: firstCard.value)
            }
            return .flush(cards: sortedCard.map{ $0.value })
        } else {
            if isConsecutive {
                return .straight(highestCardValue: firstCard.value)
            } else {
                let groups = createCardGroupsWithSortedByCountAndThenValue(cards: sortedCard)
                
                guard groups.count >= 1 else { return .invalid }
                
                if PokerHandRank.isFourOfAKind(groups: groups) {
                    return .fourOfAKind(fourCardsValue: groups[0].value)
                    
                } else if PokerHandRank.isFullHouse(groups: groups) {
                    return .fullHouse(threeCardsValue: groups[0].value)
                    
                } else if PokerHandRank.isThreeOfAKind(groups: groups) {
                    return .threeOfAKind(threeCardsValue: groups[0].value)
                    
                } else if PokerHandRank.isTwoPairs(groups: groups) {
                    return .twoPairs(highestPairCardValue: groups[0].value, anotherPairCardValue: groups[1].value, remainingCardValue: groups[2].value)
                    
                } else if PokerHandRank.isPair(groups: groups) {
                    return .pair(pairCardValue: groups[0].value, highestCardValue: groups[1].value, mediumCardValue: groups[2].value, lowestCardValue: groups[3].value)
                    
                } else if PokerHandRank.isHighCard(groups: groups) {
                     return .highCard(cards: sortedCard.map{ $0.value })
                }
            }
        }
        return .invalid
    }
    
    func sortCardsByCardValue(cards: [Card]) -> [Card] {
        return cards.sort()
    }
    
    /**
        Check if all the cards have same suit by comparing against the first card
        - Returns:
            - true - same suit
            - false - different suit
            - nil - no card
     */
    func isSameSuit(cards: [Card]) -> Bool? {
        guard cards.count > 0 else { return nil }
        
        let suit = cards[0].suit
        let invalidCards = cards.filter{$0.suit != suit}
        return invalidCards.count == 0
    }
    
    /**
        Check if all the cards are in consecutive order
        - Returns:
            - true - all are in same suit, or cards contains only 1 card
            - false - different suit
            - nil - no card
     */
    func isConsecutive(cards: [Card], startFromHighest fromHighest: Bool = true) -> Bool? {
        guard cards.count != 1 else { return true }
        guard cards.count > 1 else { return nil }

        let lastIndex = cards.count - 2
        
        for (index, card) in cards.enumerated() {
            if index > lastIndex {
                break
            }
            let nextCard = cards[index + 1]
            if !isValueConsecutive(card: card, nextCard: nextCard, startFromHighest: fromHighest) {
                return false
            }
        }
        return true
    }
    
   
    /**
        Create an array of CardGroup from array of Card
     */
    func createCardGroupsWithSortedByCountAndThenValue(cards: [Card]) -> [CardGroup] {

        let groups = cardsToKeyValuePairOfCardValueAndCardArray(cards)
        
        // Convert dictionary to array of CardGroup
        let cardGroups = groups.map{ arg -> CardGroup in
            let (key, value) = arg
            return CardGroup(cards: value, value: key, count: value.count)
        }
        
        // Sort by Count and then value in the case that their count are equal
        let sortedCardGroups = sortGroupsByCountAndThenValue(groups: cardGroups)
        return sortedCardGroups
    }
}


//MARK: - Private Methods
extension PokerCalculator {
    
    private func sortGroupsByCountAndThenValue(groups: [CardGroup]) -> [CardGroup] {
        return groups.sorted { return $0 > $1 }
    }
    /**
        Create dictionary
            key: CardValue
            value: [Card]
     */
    private func cardsToKeyValuePairOfCardValueAndCardArray(_ cards: [Card]) -> [CardValue: [Card]] {
        var groups = [CardValue: [Card]]()
        for card in cards {
            if groups[card.value] == nil {
                groups[card.value] = [card]
            } else {
                groups[card.value]?.append(card)
            }
        }
        return groups
    }
    
    private func isValueConsecutive(card: Card, nextCard: Card, startFromHighest: Bool) -> Bool {
        if (startFromHighest && !card.isGreaterThanOne(another: nextCard)) ||
            (!startFromHighest && !card.isLessThanOne(another: nextCard)) {
            return false
        }
        return true
    }
}
