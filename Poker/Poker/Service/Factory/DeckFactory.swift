//
//  DeckFactory.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation

enum DeckType {
    case deckWith52Cards
}

struct DeckFactory {
    
    static func getDeck(type: DeckType) -> DeckInterface {
        switch type {
        case .deckWith52Cards:
            let cards = CardsFactory.getCards(cardSet: CardSet.all)
            let deck = Deck()
            deck.setup(cards: cards)
            return deck
        }
    }        
}
