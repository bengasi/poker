//
//  CardsFactory.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


protocol CardsFactoryInterfce {
    static func getCards(cardSet: CardSet) -> [Card]
}

enum CardSet {
    case all
}

struct CardsFactory: CardsFactoryInterfce {
    
    static func getCards(cardSet: CardSet) -> [Card] {
        switch cardSet {
        case .all:
            let allCards =  CardValue.allValues.flatMap { (value) -> [Card] in
                let cardsWithVariedSuit = Suit.allValues.map({ (suit) -> Card in
                    return Card(suit: suit, value: value)
                })
                return cardsWithVariedSuit
            }
            return allCards
        }
    }
}
