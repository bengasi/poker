//
//  PokerCalculatorService.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


class PokerWithTwoHandsCalculatorService: PokerCalculatorServiceInterface {
    
    private let calculator: PokerCalculatorInterface

    init(calculator: PokerCalculatorInterface) {
        self.calculator = calculator
    }
    
    func calculate(pokerHands: CompetingPokerHands) -> PokerCalculationResult {
      
        guard pokerHands.count == 2 else { return PokerCalculationResult() }

        let rank1 = calculator.findPokerHandRank(cards: pokerHands[0])
        let rank2 = calculator.findPokerHandRank(cards: pokerHands[1])
        let result = calculator.detailCompare(pokerHandRank1: rank1, pokerHandRank2: rank2)
        return result
    }
}
