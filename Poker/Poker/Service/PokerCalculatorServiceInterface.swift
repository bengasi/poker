//
//  PokerCalculatorServiceInterface.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


/// This type represent Poker Hands of all players
typealias CompetingPokerHands = [[Card]]


/**
    This service perform the tasks of calculating who win the game
 */
protocol PokerCalculatorServiceInterface {
    func calculate(pokerHands: CompetingPokerHands) -> PokerCalculationResult
}
