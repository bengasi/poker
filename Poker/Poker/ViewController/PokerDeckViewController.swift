//
//  PokerDeckViewController.swift
//  Poker
//
//  Created by Atikom Tancharoen on 26/1/18.
//  Copyright © 2018 ben. All rights reserved.
//

import UIKit


protocol PokerDeckViewControllerDelegate: class {
    func pokerHandDidUpdate(cards: [Card?], playerIndex: Int)
}


class PokerDeckViewController: UIViewController {
  
    @IBOutlet private weak var cardView1: CardView!
    @IBOutlet private weak var cardView2: CardView!
    @IBOutlet private weak var cardView3: CardView!
    @IBOutlet private weak var cardView4: CardView!
    @IBOutlet private weak var cardView5: CardView!
    
    private var viewModel: PokerDeckInteractorInterface?
    weak var delegate: PokerDeckViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func configure(viewModel: PokerDeckInteractorInterface) {
        self.viewModel = viewModel
        bindToViewModel()
    }
    
    func didUpdateSelectedCard() -> ((Int) -> Void) {
        return { [weak self] (index: Int )in
            guard let weakSelf = self else { return }
            guard let vm = weakSelf.viewModel else { return }
            guard let card = vm.output.cards[index] else { return }
            
            if vm.output.cardIndex == 0 {
                weakSelf.cardView1.configure(valueText: card.valueText, imageName: card.imageName)
            } else if vm.output.cardIndex == 1 {
                weakSelf.cardView2.configure(valueText: card.valueText, imageName: card.imageName)
            } else if vm.output.cardIndex == 2 {
                weakSelf.cardView3.configure(valueText: card.valueText, imageName: card.imageName)
            } else if vm.output.cardIndex == 3 {
                weakSelf.cardView4.configure(valueText: card.valueText, imageName: card.imageName)
            } else if vm.output.cardIndex == 4 {
                weakSelf.cardView5.configure(valueText: card.valueText, imageName: card.imageName)
            }
        }
    }
    
    @IBAction func cardValueTapped(_ sender: UIButton) {
        guard let vm = viewModel else { return }

        if let value = CardValue(rawValue: sender.tag % 100) {
            let suit = Suit(tag: sender.tag / 100)
            let card = Card(suit: suit, value: value)
            vm.input.updateCard(card: card)
        }
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        
        let notSelectedCards = vm.output.cards.filter{ $0 == nil }
        guard notSelectedCards.count == 0 else {
            
            let alert = UIAlertController(title: "Plese select 5 cards", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        delegate?.pokerHandDidUpdate(cards: vm.output.cards, playerIndex: vm.output.playerIndex)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cardView1Tapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        
        resetCardViewColor()
        cardView1.backgroundColor = UIColor.lightGray
        vm.input.updateCardIndex(0)
    }
    
    @IBAction func cardView2Tapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        
        resetCardViewColor()
        cardView2.backgroundColor = UIColor.lightGray
        vm.input.updateCardIndex(1)
    }
    
    @IBAction func cardView3Tapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        
        resetCardViewColor()
        cardView3.backgroundColor = UIColor.lightGray
        vm.input.updateCardIndex(2)
    }
    
    @IBAction func cardView4Tapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        
        resetCardViewColor()
        cardView4.backgroundColor = UIColor.lightGray
        vm.input.updateCardIndex(3)
    }
    
    @IBAction func cardView5Tapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        
        resetCardViewColor()
        cardView5.backgroundColor = UIColor.lightGray
        vm.input.updateCardIndex(4)
    }
 }


//MARK: - Private methods
extension PokerDeckViewController {
    
    private func setupUI() {
        cardView1.backgroundColor = UIColor.lightGray
        
        cardView1.configure(valueText: "", imageName: "")
        cardView2.configure(valueText: "", imageName: "")
        cardView3.configure(valueText: "", imageName: "")
        cardView4.configure(valueText: "", imageName: "")
        cardView5.configure(valueText: "", imageName: "")
    }
    
    private func bindToViewModel() {
        viewModel?.output.didUpdateSelectedCard = didUpdateSelectedCard()
    }
    
    private func resetCardViewColor() {
        guard let vm = viewModel else { return }
        if vm.output.cardIndex == 0 {
            cardView1.backgroundColor = UIColor.white
        } else if vm.output.cardIndex == 1 {
            cardView2.backgroundColor = UIColor.white
        } else if vm.output.cardIndex == 2 {
            cardView3.backgroundColor = UIColor.white
        } else if vm.output.cardIndex == 3 {
            cardView4.backgroundColor = UIColor.white
        } else if vm.output.cardIndex == 4 {
            cardView5.backgroundColor = UIColor.white
        }
    }
}


extension Suit {
    
    init(tag: Int) {
        if tag == 1 {
            self = .clubs
        } else if tag == 2 {
            self = .diamonds
        } else if tag == 3 {
            self = .hearts
        } else {
            self = .spades
        }
    }
}

