//
//  PokerBoardViewController.swift
//  Poker
//
//  Created by ben on 1/25/18.
//  Copyright © 2018 ben. All rights reserved.
//

import UIKit

class PokerBoardViewController: UIViewController {
    
    @IBOutlet private var player1Cards: [CardView]!
    @IBOutlet private var player2cards: [CardView]!
    @IBOutlet private weak var gameResultLabel: UILabel!
    
    @IBOutlet private weak var player1Label: UILabel!
    @IBOutlet private weak var player2Label: UILabel!
    
    var viewModel: PokerBoardInteractorInterface?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let calculator = PokerCalculator()
        let calService = PokerWithTwoHandsCalculatorService(calculator: calculator)
        
        let players = preparePlayer()
        
        configure(viewModel: PokerBoardViewModel(service: calService, player1: players.p1, player2: players.p2))
        
        setupUI()
    }
    
    func configure(viewModel: PokerBoardInteractorInterface) {
        self.viewModel = viewModel
        bindToViewModel()
    }

    func didUpdateCalculation() -> (() -> Void) {
        return { [weak self] in
            guard let weakSelf = self else { return }
            guard let vm = weakSelf.viewModel else { return }
            let text = vm.output.getGameResult()
            weakSelf.gameResultLabel.text = text
        }
    }
    
    func didUpdatePlayerPokerHand() -> ((Int) -> Void) {
        return { [weak self] (index: Int) in
            guard let weakSelf = self else { return }

            weakSelf.renderCardForPlayer(index: index)
        }
    }
    
    @IBAction func player1DealTapped(_ sender: Any) {
        presentDeck(playerIndex: 0)
    }
    
    @IBAction func player2DealTapped(_ sender: Any) {
        presentDeck(playerIndex: 1)
    }

    @IBAction func calculateTapped(_ sender: Any) {
        guard let vm = viewModel else { return }
        vm.input.calculatePokerHand()
    }
}


//MARK: - Private methods
extension PokerBoardViewController {
    
    private func preparePlayer() -> (p1: PokerPlayer, p2: PokerPlayer) {
        let player1 = PokerPlayer(id: "0001")
        player1.name = "Player1"

        let player2 = PokerPlayer(id: "0002")
        player2.name = "Player2"

        return (player1, player2)
    }
    
    private func setupUI() {
        guard let vm = viewModel else { return }
        player1Label.text = vm.output.getPlayer(index: 0).name
        player2Label.text = vm.output.getPlayer(index: 1).name
        
        _ = player1Cards.map { (cardView)  in
            cardView.configure(valueText: "", imageName: "")
        }
        _ = player2cards.map { (cardView)  in
            cardView.configure(valueText: "", imageName: "")
        }
    }
    
    private func bindToViewModel() {
        viewModel?.output.didUpdateCalculation = didUpdateCalculation()
        viewModel?.output.didUpdatePlayerPokerHand = didUpdatePlayerPokerHand()
    }
    
    private func renderCardForPlayer(index: Int) {
        let cards = index == 0 ? player1Cards: player2cards
        
        guard let cardViews = cards else { return }
        
        guard let vm = viewModel else { return }
        
        let cardInfos = vm.output.getCardInfo(playerIndex: index)
        
        for (index, cardView) in cardViews.enumerated() {
            let card = cardInfos[index]
            cardView.configure(valueText: card.valueText, imageName: card.imageName)
        }
    }
    
    private func presentDeck(playerIndex: Int) {
        let storyboard = UIStoryboard.init(name: "PokerDeck", bundle: Bundle.main)
        if let vc = storyboard.instantiateInitialViewController() as? PokerDeckViewController {
            vc.configure(viewModel: PokerDeckViewModel(playerIndex: playerIndex))
            vc.delegate = self
            present(vc, animated: true, completion: nil)
        }
    }    
}


extension PokerBoardViewController: PokerDeckViewControllerDelegate {
    func pokerHandDidUpdate(cards: [Card?], playerIndex: Int) {
        guard let vm = viewModel else { return }
        print("playerIndex \(playerIndex) cards \(cards)")
        let nonOptionalCards = cards.filter{$0 != nil }.map{$0!}
        vm.input.updateCardsForPlayer(index: playerIndex, cards: nonOptionalCards)
    }    
}
