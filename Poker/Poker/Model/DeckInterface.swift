//
//  DeckInterface.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


/**
    This protocol represents the card deck
 */
protocol DeckInterface {
    
    var expectedCardsCount: Int { get }
    
    /// All cards in the deck
    var cards: [Card] { get }
    
    /// Setup all the cards. Invoke this at the beginning to initialize the cards
    func setup(cards c: [Card])

    /**
        dealt one card from a deck in case it's not dealt.
     
        - Returns:
            - Return nil in case the card was dealt.
            - Return a Card instance if it was not deal
     */
    func dealCard(suit: Suit, value: CardValue) -> Card?
    
    /// Check if the card was dealt
    func isCardDealt(card: Card) -> Bool
    
    /// Get all undealt cards from the deck
    func getUndealtCards() -> [Card]
}
