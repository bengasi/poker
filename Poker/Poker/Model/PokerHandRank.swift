//
//  PokerHandRank.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


enum PokerHandRank: Equatable {
    
    /// Hands which do not fit any higher category are ranked by the value of their highest card. If the highest cards have the same value, the hands are ranked by the next highest, and so on.
    case highCard(cards: [CardValue])
    
    /// 2 of the 5 cards in the hand have the same value. Hands which both contain a pair are ranked by the value of the cards forming the pair. If these values are the same, the hands are ranked by the values of the cards not forming the pair, in decreasing order.
    case pair(pairCardValue: CardValue, highestCardValue: CardValue, mediumCardValue: CardValue, lowestCardValue: CardValue)
    
    /// The hand contains 2 different pairs. Hands which both contain 2 pairs are ranked by the value of their highest pair. Hands with the same highest pair are ranked by the value of their other pair. If these values are the same the hands are ranked by the value of the remaining card.
    case twoPairs(highestPairCardValue: CardValue, anotherPairCardValue: CardValue, remainingCardValue: CardValue )
    
    /// Three of the cards in the hand have the same value. Hands which both contain three of a kind are ranked by the value of the 3 cards.
    case threeOfAKind(threeCardsValue: CardValue)
    
    /// Hand contains 5 cards with consecutive values. Hands which both contain a straight are ranked by their highest card.
    case straight(highestCardValue: CardValue)
    
    /// Hand contains 5 cards of the same suit. Hands which are both flushes are ranked using the rules for High Card
    case flush(cards: [CardValue])
    
    /// 3 cards of the same value, with the remaining 2 cards forming a pair. Ranked by the value of the 3 cards.
    case fullHouse(threeCardsValue: CardValue)
    
    /// 4 cards with the same value. Ranked by the value of the 4 cards.
    case fourOfAKind(fourCardsValue: CardValue)
    
    /// 5 cards of the same suit with consecutive values
    case straightFlush(highestCardValue: CardValue)
    
    case invalid
    
    
    var description: String {
        switch self {
        case .highCard:     return "High Card"
        case .pair:         return "Pair"
        case .twoPairs:     return "Two Pairs"
        case .threeOfAKind: return "Three of a Kind"
        case .straight:     return "Straight"
        case .flush:        return "Flush"
        case .fullHouse:    return "Full House"
        case .fourOfAKind:  return "Four of a Kind"
        case .straightFlush: return "Straight Flush"
        case .invalid:        return "Invalid"
        }
    }
    
    var order: Int {
        switch self {
        case .highCard:     return 1
        case .pair:         return 2
        case .twoPairs:     return 3
        case .threeOfAKind: return 4
        case .straight:     return 5
        case .flush:        return 6
        case .fullHouse:    return 7
        case .fourOfAKind:  return 8
        case .straightFlush: return 9
        case .invalid:        return -1
        }
    }
    
    static func isHighCard(groups: [CardGroup]) -> Bool {
        return  groups.count == 5
    }
    
    static func isPair(groups: [CardGroup]) -> Bool {
        return  groups.count == 4 &&
                groups[0].count == 2 &&
                groups[1].count == 1 &&
                groups[2].count == 1 &&
                groups[3].count == 1
    }
    static func isTwoPairs(groups: [CardGroup]) -> Bool {
        return  groups.count == 3 &&
                groups[0].count == 2 &&
                groups[1].count == 2 &&
                groups[2].count == 1
    }
    static func isThreeOfAKind(groups: [CardGroup]) -> Bool {
        return groups.count == 3 &&
                groups[0].count == 3 &&
                groups[1].count == 1 &&
                groups[2].count == 1
    }
    
    static func isFullHouse(groups: [CardGroup]) -> Bool {
        return groups.count == 2 &&
                groups[0].count == 3 &&
                groups[1].count == 2
    }
    
    static func isFourOfAKind(groups: [CardGroup]) -> Bool {
        return  groups.count == 2 &&
                groups[0].count == 4 &&
                groups[1].count == 1
    }
    
    /**
        Compare self to another PokerHandRank
        - Returns: PokerComparisonResult
     */
    func compareToAnother(another: PokerHandRank) -> PokerComparisonResult {
        // Rely on order first for the same Poker Hand Rank Type
        if self.order > another.order {
            return .win
        } else if order < another.order {
            return .lose
        }
        
        // For same Poker Hand Rank Type, further calculate
        return compareSamePokerHandRank(another: another)
    }
    
    /**
        Compare self to another PokerHandRank and return the detail of comparision
        - Returns: PokerCalculationResult
     */
    func compareToAnotherAndGetDetail(another: PokerHandRank) -> PokerCalculationResult {
        // Rely on order first for the same Poker Hand Rank Type
        var pokerCalResult = PokerCalculationResult()
        pokerCalResult.winAttributes = nil
        
        if self.order > another.order {
            pokerCalResult.comparisionResult = .win
            pokerCalResult.winPokerHandRank = self
            pokerCalResult.losePokerHandRank = another
            return pokerCalResult
            
        } else if order < another.order {
            pokerCalResult.comparisionResult = .lose
            pokerCalResult.winPokerHandRank = another
            pokerCalResult.losePokerHandRank = self
            return pokerCalResult
            
        } else {            
            // For same Poker Hand Rank Type, further calculate
            return compareSamePokerHandRankAndGetDetail(another: another)
        }
    }
    
    /**
        Compare PokerHandRank in the case that they have same Rank type
     
        For example:
        - Both Poker Hand rank are Full House, in which the first one contains 3 cards of K
        and the second contains 3 cards of J
     
        - Both Poker Hand rank are Full House, in which the first one contains 3 cards of K, and 2 cards of Five and the second also contains 3 cards of K but 2 cards of Four
     */
    func compareSamePokerHandRank(another: PokerHandRank) -> PokerComparisonResult {
        
        switch (self, another) {
            
        case let (.invalid, .invalid):
            return .tie
            
        case let (.highCard(lCards), .highCard(rCards)):
            guard lCards.count == rCards.count else { return .invalid }
            return compareListAndGetDetail(lList: lCards, rList: rCards).comparison
            
        case let (.pair(lPairCardValue, lHighestCardValue, lMediumCardValue, lLowestCardValue),
                  .pair(rPairCardValue, rHighestCardValue, rMediumCardValue, rLowestCardValue)):
            return compareListAndGetDetail(lList: [lPairCardValue, lHighestCardValue, lMediumCardValue, lLowestCardValue],
                               rList: [rPairCardValue, rHighestCardValue, rMediumCardValue, rLowestCardValue]).comparison
            
        case let (.twoPairs(lHighestPairCardValue, lAnotherPairCardValue, lRemainingCardValue),
                  .twoPairs(rHighestPairCardValue, rAnotherPairCardValue, rRemainingCardValue)):
            return compareListAndGetDetail(lList: [lHighestPairCardValue, lAnotherPairCardValue, lRemainingCardValue],
                               rList: [rHighestPairCardValue, rAnotherPairCardValue, rRemainingCardValue]).comparison
            
        case let (.threeOfAKind(lThreeCardsValue), .threeOfAKind(rThreeCardsValue)):
            return compare(lhs: lThreeCardsValue, rhs: rThreeCardsValue)
            
        case let (.straight(lHighestValue), .straight(rHighestValue)):
            return compare(lhs: lHighestValue, rhs: rHighestValue)
            
        case let (.flush(lCards), .flush(rCards)):
            guard lCards.count == rCards.count else { return .invalid }
            return compareListAndGetDetail(lList: lCards, rList: rCards).comparison
            
        case let (.fullHouse(lThreeCardsValue), .fullHouse(rThreeCardsValue)):
            return compare(lhs: lThreeCardsValue, rhs: rThreeCardsValue)
            
        case let (.fourOfAKind(lFourCardsValue), .fourOfAKind(rFourCardsValue)):
            return compare(lhs: lFourCardsValue, rhs: rFourCardsValue)
            
        case let (.straightFlush(lHighestCardValue), .straightFlush(rHighestCardValue)):
            return compare(lhs: lHighestCardValue, rhs: rHighestCardValue)
            
        default: return .invalid
        }
    }
}


//MARK: - private/fileprivate Methods
extension PokerHandRank {
    
    private func compareSamePokerHandRankAndGetDetail(another: PokerHandRank) -> PokerCalculationResult {
        
        switch (self, another) {
            
        case let (.highCard(lCards), .highCard(rCards)):
            guard lCards.count == rCards.count else { return PokerCalculationResult() }
            return createPokerCalculationResultFromArray(lRank: self, rRank: another, lCardValues: lCards, rCardValues: rCards)
            
        case let (.pair(lPairCardValue, lHighestCardValue, lMediumCardValue, lLowestCardValue),
                  .pair(rPairCardValue, rHighestCardValue, rMediumCardValue, rLowestCardValue)):
            let lList = [lPairCardValue, lHighestCardValue, lMediumCardValue, lLowestCardValue]
            let rList = [rPairCardValue, rHighestCardValue, rMediumCardValue, rLowestCardValue]
            return createPokerCalculationResultFromArray(lRank: self, rRank: another, lCardValues: lList, rCardValues: rList)
            
        case let (.twoPairs(lHighestPairCardValue, lAnotherPairCardValue, lRemainingCardValue),
                  .twoPairs(rHighestPairCardValue, rAnotherPairCardValue, rRemainingCardValue)):
            let lList = [lHighestPairCardValue, lAnotherPairCardValue, lRemainingCardValue]
            let rList = [rHighestPairCardValue, rAnotherPairCardValue, rRemainingCardValue]
            return createPokerCalculationResultFromArray(lRank: self, rRank: another, lCardValues: lList, rCardValues: rList)
            
        case let (.threeOfAKind(lThreeCardsValue), .threeOfAKind(rThreeCardsValue)):
            return createPokerCalculationResult(lRank: self, rRank: another, lCardValue: lThreeCardsValue, rCardValue: rThreeCardsValue)
            
        case let (.straight(lHighestValue), .straight(rHighestValue)):
            return createPokerCalculationResult(lRank: self, rRank: another, lCardValue: lHighestValue, rCardValue: rHighestValue)
            
        case let (.flush(lCards), .flush(rCards)):
            guard lCards.count == rCards.count else { return PokerCalculationResult() }
            
            return createPokerCalculationResultFromArray(lRank: self, rRank: another, lCardValues: lCards, rCardValues: rCards)
            
        case let (.fullHouse(lThreeCardsValue), .fullHouse(rThreeCardsValue)):
            return createPokerCalculationResult(lRank: self, rRank: another, lCardValue: lThreeCardsValue, rCardValue: rThreeCardsValue)
            
        case let (.fourOfAKind(lFourCardsValue), .fourOfAKind(rFourCardsValue)):
            return createPokerCalculationResult(lRank: self, rRank: another, lCardValue: lFourCardsValue, rCardValue: rFourCardsValue)
            
        case let (.straightFlush(lHighestCardValue), .straightFlush(rHighestCardValue)):
            return createPokerCalculationResult(lRank: self, rRank: another, lCardValue: lHighestCardValue, rCardValue: rHighestCardValue)
            
        default: return PokerCalculationResult()
        }
    }
    
    /// compare a value of CardValue and then return the result in PokerComparisonResult
    private func compare(lhs: CardValue, rhs: CardValue) -> PokerComparisonResult {
        return CardValue.compare(lhs: lhs, rhs: rhs).toPokerComparasionResult()
    }
    
    private typealias CardValueToBeCompared = (CardValue, CardValue)      
    
    /**
        Compare the list of CardValues
        - Returns:
            (comparison: PokerComparisonResult, cardValuePair: CardValueToBeCompared?)
     */
    private func compareListAndGetDetail(lList: [CardValue], rList: [CardValue]) -> (comparison: PokerComparisonResult, cardValuePair: CardValueToBeCompared?) {
        
        for (index, value) in lList.enumerated() {
            if value == rList[index] {
                continue
            }
            let result = compare(lhs: value, rhs: rList[index])
            return (result, (value, rList[index]))
        }
        return (.tie, nil)
    }
    
    private func createPokerCalculationResult(lRank: PokerHandRank,
                                              rRank: PokerHandRank,
                                              lCardValue: CardValue,
                                              rCardValue: CardValue) -> PokerCalculationResult {
        let comparision = compare(lhs: lCardValue, rhs: rCardValue)
        return PokerCalculationResult(comparison: comparision,
                                      lRank: lRank,
                                      rRank: rRank,
                                      lCardValue: lCardValue,
                                      rCardValue: rCardValue)
    }
    
    private func createPokerCalculationResultFromArray(lRank: PokerHandRank,
                                                       rRank: PokerHandRank,
                                                       lCardValues: [CardValue],
                                                       rCardValues: [CardValue]) -> PokerCalculationResult {
        let result = compareListAndGetDetail(lList: lCardValues, rList: rCardValues)
        
        return PokerCalculationResult(comparison: result.comparison,
                                      lRank: lRank,
                                      rRank: rRank,
                                      lCardValue: result.cardValuePair?.0,
                                      rCardValue: result.cardValuePair?.1)
    }
}

func ==(lhs: PokerHandRank, rhs: PokerHandRank) -> Bool {
    return (lhs.order == rhs.order) && lhs.compareSamePokerHandRank(another: rhs) == .tie
}

/**
    Greater order means
    - Higher Rank
    - Same Rank but get higher on further calculation
 */
func >(lhs: PokerHandRank, rhs: PokerHandRank) -> Bool {
    return lhs.compareToAnother(another: rhs) == .win
}

func <(lhs: PokerHandRank, rhs: PokerHandRank) -> Bool {
    return lhs.compareToAnother(another: rhs) == .lose
}

