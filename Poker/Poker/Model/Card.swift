//
//  Card.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


/**
    This Enum represents the card value
 */
enum CardValue: Int, Equatable {
    case two = 2
    case three = 3
    case four = 4
    case five = 5
    case six = 6
    case seven = 7
    case eight = 8
    case nine = 9
    case T = 10
    case J = 11
    case Q = 12
    case K = 13
    case A = 14
    
    static func compare(lhs: CardValue, rhs: CardValue) -> ComparisonResult {
        if lhs == rhs {
            return .orderedSame
        } else if lhs < rhs {
            return .orderedAscending
        }
        return .orderedDescending
    }
    
    var shownText: String {
        switch self {
        case .two, .three, .four, .five, .six, .seven, .eight, .nine:
            return String(self.rawValue)
        case .T: return "10"
        case .J: return "J"
        case .Q: return "Q"
        case .K: return "K"
        case .A: return "A"
        }
    }
}

func ==(lhs: CardValue, rhs: CardValue) -> Bool {
    return lhs.rawValue == rhs.rawValue
}

func >(lhs: CardValue, rhs: CardValue) -> Bool {
    return lhs.rawValue > rhs.rawValue
}

func <(lhs: CardValue, rhs: CardValue) -> Bool {
    return lhs.rawValue < rhs.rawValue
}


//MARK: - CardElement
extension CardValue: CardElement {
    static var allValues: [CardValue] {
        return [.two, .three, .four, .five, .six, .seven, .eight, .nine, .T,
                .J, .Q, .K, .A]
    }
}


/**
    This represents a card in which contains a suit and value
 */
struct Card: Equatable, CustomStringConvertible {
    let suit: Suit
    let value: CardValue
    
    init(suit: Suit, value: CardValue) {
        self.suit = suit
        self.value = value
    }
        
    var description: String {
        return "\n Card: \(value)-\(suit)"
    }
    
    /// Consider only value, not suit
    func isGreaterThanOne(another: Card) -> Bool {
        return value.rawValue == another.value.rawValue + 1
    }
    
    /// Consider only value, not suit
    func isLessThanOne(another: Card) -> Bool {
        return value.rawValue == another.value.rawValue - 1
    }
}

func ==(lhs: Card, rhs: Card) -> Bool {
    if lhs.suit == rhs.suit && lhs.value == rhs.value {
        return true
    }
    return false
}

/// Consider only value
func >(lhs: Card, rhs: Card) -> Bool {
    return lhs.value > rhs.value
}

/// Consider only value
func <(lhs: Card, rhs: Card) -> Bool {
    return lhs.value < rhs.value
}
