//
//  Suit.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation

/**
    This Enum represents a suit of card
 */
enum Suit: CustomStringConvertible {
    case clubs
    case diamonds
    case hearts
    case spades
    
    var description: String {
        switch self {
        case .clubs:
            return "Clubs"
        case .diamonds:
            return "Diamonds"
        case .hearts:
            return "Hearts"
        case .spades:
            return "Spades"
        }
    }
}


//MARK: - CardElement
extension Suit: CardElement {
    static var allValues: [Suit] {
        return [.clubs, .diamonds, .hearts, .spades]
    }
}
