//
//  PokerHand.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


class PokerHand: PokerHandInterface {
    
    var cards: [Card] = []
    
    /**
        Add card to the deck with the conditions
        - Poker Hand contains at most 5 cards
        - Cannot add duplicated card
     
        - Returns: Return if adding card is succes or not
     */
    
    func addCard(card: Card) -> Bool {
        guard cards.count < 5 else { return false }
        
        guard cards.index(of: card) == nil else { return false }
        
        cards.append(card)
        return true
    }

    func addCards(cards: [Card]) -> Bool {
        let newCardCount = cards.count
        guard self.cards.count + newCardCount <= 5 else { return false }
        self.cards.append(contentsOf: cards)
        return true
    }
    
    func removeCard(card: Card) {
        if let matchedIndex = cards.index(of: card) {
            cards.remove(at: matchedIndex)
        }
    }
    
    func removeAllCards() {
        cards = []
    }
}
