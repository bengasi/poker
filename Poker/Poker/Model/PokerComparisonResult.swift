//
//  PokerComparisonResult.swift
//  Poker
//
//  Created by ben on 18/3/2561 BE.
//  Copyright © 2561 ben. All rights reserved.
//

import Foundation


enum PokerComparisonResult: CustomStringConvertible {
    case win
    case lose
    case tie
    case invalid
    
    var description: String {
        switch self {
        case .win:
            return "win"
        case .lose:
            return "lose"
        case .tie:
            return "tie"
        default:
            return "invalid"
        }
    }
}
