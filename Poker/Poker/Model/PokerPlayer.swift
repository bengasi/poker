//
//  PokerPlayer.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


class PokerPlayer {
    var id: String
    var name: String = "Unknown"
    var pokerHand: PokerHandInterface
    
    init(id: String) {
        self.id = id
        self.pokerHand = PokerHand()
    }
}
