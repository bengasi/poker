//
//  PokerCalculationResult.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


struct PokerCalculationResult: CustomStringConvertible{
    
    var comparisionResult: PokerComparisonResult?
    
    var winPokerHandRank: PokerHandRank?
    var losePokerHandRank: PokerHandRank?
    
    var winIndex: Int {
        if comparisionResult == .win {
            return 0
        } else if comparisionResult == .lose {
            return 1
        }
        return -1
    }
    
    var winAttributes: CardValue?
    
    var isSameRankType: Bool {
        guard let win = winPokerHandRank, let lose = losePokerHandRank else { return false }
        
        return win.order == lose.order
    }
    
    init() {
    }
    
    var description: String {
        return ("POKER RESULT: \(String(describing: comparisionResult)) winIndex: \(winIndex) attr: \(String(describing: winAttributes)) isSameRank: \(isSameRankType) winner: \(String(describing: winPokerHandRank)) loser: \(String(describing: losePokerHandRank))")
    }
}


extension PokerCalculationResult {
    
    init(comparison: PokerComparisonResult,
         lRank: PokerHandRank, rRank: PokerHandRank,
         lCardValue: CardValue?, rCardValue: CardValue?) {
        
        self = PokerCalculationResult()
        
        if case .win = comparison {
            comparisionResult = comparison
            winPokerHandRank = lRank
            winAttributes = lCardValue
            losePokerHandRank = rRank
            
        } else if case .lose = comparison {
            comparisionResult = comparison
            winPokerHandRank = rRank
            winAttributes = rCardValue
            losePokerHandRank = lRank
            
        } else if case .tie = comparison {            
            comparisionResult = comparison
            winPokerHandRank = lRank
            winAttributes = lCardValue
            losePokerHandRank = lRank
        }
    }
}
