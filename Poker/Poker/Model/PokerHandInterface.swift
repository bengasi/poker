//
//  PokerHandInterface.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


protocol PokerHandInterface {
    var cards: [Card] { get }
    
    func addCard(card: Card) -> Bool
    func addCards(cards: [Card]) -> Bool
    
    func removeCard(card: Card)
    func removeAllCards()
}
