//
//  Deck.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


class Deck: DeckInterface {
    
    var expectedCardsCount: Int {
        return 52
    }
    
    var cards: [Card] = []
    var dealtCards: [Card] = []
    
    func setup(cards c: [Card]) {
        cards = c
    }
    
    func dealCard(suit: Suit, value: CardValue) -> Card? {
        let card = Card(suit: suit, value: value)

        guard let matchedIndex = cards.index(of: card) else { return nil }
        
        cards.remove(at: matchedIndex)
        dealtCards.append(card)
        
        return card
    }
    
    func isCardDealt(card: Card) -> Bool {
        if let _ = dealtCards.index(of: card) {
            return true
        }
        return false
    }
    
    func getUndealtCards() -> [Card] {
        return cards
    }
}
