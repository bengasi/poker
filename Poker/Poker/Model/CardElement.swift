//
//  CardElement.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


/**
    This represents the Element of cards like CardValue and Suit.
    It helps while creating an instance of Card
 */
protocol CardElement {
    associatedtype Element
    static var allValues: Element { get }
}
