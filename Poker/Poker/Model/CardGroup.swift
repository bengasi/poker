//
//  CardGroup.swift
//  Poker
//
//  Created by ben on 1/21/18.
//  Copyright © 2018 ben. All rights reserved.
//

import Foundation


/**
    This represent of the cards that are grouped within Poker Hand according to their card value
    - Example:
    For the Poker Hand that contains:
        4 clubs, 4 diamonds, 4 hearts, 2 clubs, 1 spades
        CardGroup should be created with the value of 4 and count of 3
 */
struct CardGroup {
    var cards: [Card]
    var value: CardValue
    var count: Int
    
    func isSameCount(_ anotherGroup: CardGroup) -> Bool {
        return count == anotherGroup.count
    }
}

func >(lhs: CardGroup, rhs: CardGroup) -> Bool {
    if lhs.count > rhs.count {
        return true
    } else if lhs.isSameCount(rhs) {
        return lhs.value.rawValue > rhs.value.rawValue
    }
    return false
}

