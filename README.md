# Poker

Poker Hand Calculation


## Getting Started

Related to the document of the application, we provide the documentation in the code for developers, and also provide the user document as a manual of how to play it.

### How it looks

Poker Hand Calculation consists of 2 screens

- **Poker Board Screen**
- **Deck Board Screen **

**Poker Board Screen**
This is the 1st screen of the application. It shows **Poker Hand** of two players. Poker Hand on top of the screen belongs to the 1st player, and the one at the bottom belongs to the 2nd player. Poker Hand consists of 5 cards in which all are empty at the beginning. To fulfill it, we have to select the cards for each player one by one.
Tap on the *Select Card* button  below the cards to open **Deck Board Screen** to select the cards for each player. It will be explained in the next step.

**Deck Board Screen **
Open Deck Board Screen to select the cards. Once you open it, you will see all five cards are empty. Select an empty card (The first card is selected by default), and then select a **Card Value** for a **Suit** shown as a list. Once you select the Card Value and Suit of the first card, tap a next empty card and repeat the same step until 5 cards are filled.

On this screen, you can discard the selection by tapping *Cancel* button, or can save the cards for further calculation by tapping *Done* button. Note that if all 5 cards are not filled at the time you tap *Done*, the application will not allow it.

Once you have done with the cards seletion for 2 players, it's time to see the calculation result. On Poker Board Screen, tap *Calculate* button. The calculation result will be shown as a description in the middle of the screen. The below text are the examples of the calculation result.

```
"Player1 win with Four of a Kind: 9"

"Player1 win with Straight Flush"

"Tie with Pair"

```

## Unit Test

The application includes Unit Test for the calculation logic. The implementation starts from designing how to calculate the result, and then design the whole picture of what components should be existed, and how they interact with each other. We have written Unit Test for the correctness of the logic and also to make sure that the change during a development will not effect the already-work-functionalities.


## Limitation

With time constraint we appologized that the application is not completed in the way that it not prevent the user from invalid input. It's possible to cause an error.


## Built With

- XCode
- SourceTree

## Authors

- Ben


## Acknowledgments

- The challenge of the problem
- [Source of Input/Output for Unit Test](https://github.com/secnot/uva-onlinejudge-solutions/blob/master/10315%20-%20Poker%20Hands/input.txt)
- etc.
